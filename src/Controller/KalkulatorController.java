package Controller;

import Model.KalkulatorModel;
import View.KalkulatorView;

import java.util.Scanner;

public class KalkulatorController {

    private KalkulatorModel model;

    private KalkulatorView view;

    public KalkulatorController(KalkulatorModel model, KalkulatorView view){
        this.model = model;
        this.view = view;
    }

    public void kalkulator(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukan Input : ");
        String input = sc.nextLine();

        try {
            model.kalkulator(input);
            view.showHasil(model.getHasil());
        } catch (ArithmeticException e) {
            view.showError("Tidak bisa dilakukan");
        } catch (IllegalArgumentException e) {
            view.showError("Operator gagal ditemukan");
        }
    }

}
