package View;

public class KalkulatorView {

    public void showHasil(int hasil){
        System.out.println("Output : " + hasil);
    }

    public void showError(String msg){
        System.out.println("Error " + msg);
    }

}
