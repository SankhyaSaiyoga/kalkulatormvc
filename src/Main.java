import Controller.KalkulatorController;
import Model.KalkulatorModel;
import View.KalkulatorView;

public class Main {
    public static void main(String[] args) {
        KalkulatorModel model = new KalkulatorModel();
        KalkulatorView view = new KalkulatorView();
        KalkulatorController controller = new KalkulatorController(model, view);


        controller.kalkulator();
    }
}