package Model;

import java.util.Scanner;

public class KalkulatorModel {

    public int hasil;

    public void kalkulator(String input){
        Scanner sc = new Scanner(input);
        int num1 = sc.nextInt();
        String operator = sc.next();
        int num2 = sc.nextInt();

        switch (operator) {
            case "+":
                hasil = num1 + num2;
                break;
            case "-":
                hasil = num1 - num2;
                break;
            case "x":
                hasil = num1 * num2;
                break;
            case "/":
                if (num2 == 0){
                    throw new ArithmeticException("Tidak bisa dilakukan");
                }
                hasil = num1 / num2;
                break;
            default:
                throw new IllegalArgumentException("Operator gagal ditemukan");
        }
    }

    public int getHasil(){
        return hasil;
    }

}
